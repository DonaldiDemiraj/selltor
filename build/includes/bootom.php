<div class="grid grid-cols-1 lg:grid-cols-4 bg-primary lg:px-16 px-4 pb-4 pt-6">
    <div class="">
        <img src="img/logo_big.svg" alt="Sellter" class="w-32">

        <p class="text-white text-md pt-6">
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Asperiores dicta sapiente nesciunt quos ex magni.
        </p>
    </div>

    <div class="lg:col-span-2 lg:text-right pt-8 pr-64">
        <p class="font-bold text-white mb-4">Quick Link</p>
        <div class=""underline>
            <p><a href="#" class="text-accent font-bold text-base">About us</a></p>
            <p><a href="#" class="text-accent font-bold text-base">Services</a></p>
            <p><a href="#" class="text-accent font-bold text-base">Team</a></p>
        </div>
    </div>

    <div class="pt-6 ml-4">
        <p class="font-bold text-white mb-4">Contact Us</p>
        <a href="#" class="rounded-full bg-white bgg-opacity-50 py-4 px-8 text-white text-sm flex items-center mb-4">
            <img src="img/mail_icon.svg" alt="" class="h-4 w-4 mr-4">
            <p class="text-black">email@selltor.com</p>
        </a>
        <a href="#" class="rounded-full bg-white bgg-opacity-50 py-4 px-8 text-white text-sm flex items-center mb-4">
            <img src="img/phone_icon.svg" alt="" class="h-4 w-4 mr-4">
            <p class="text-black">Phone: +355 67 34 43 444</p>
        </a>
        <a href="#" class="rounded-full bg-white bgg-opacity-50 py-4 px-8 text-white text-sm flex items-center mb-4">
            <img src="img/address_icon.svg" alt="" class="h-4 w-4 mr-4">
            <p class="text-black">Address: St.Trenit Tirane ALBANIA</p>
        </a>
    </div>
</div>