<div class="hidden modal-container h-full w-full fixed top-0 left-0 flex items-center justify-center">  
        <div class="modal-overlay z-0 absolute w-full h-full top-0 left-0 bg-gray-900 opacity-70 overflow-y-auto"></div>
        <div class="modal-content">
            <div class="hidden register-form grid lg:grid-cols-2">
                <div class="hidden lg:inline my-4 pt-64 px-16 relative rounded-lg shadow z-0 bg-cover bg-no-repeat" style="background-image:url('img/property_img_sample_1.png'); left:50;">
                    <div class="text-center">
                        <a href="#" class="google-button mb-4 bg-white shadow rounded flex justify-center px-8 py-2 text-gray-900 mx-8">
                            <img src="img/google_icon.svg" alt="" class="mr-4">
                            <p class="text-xs">Sign In With Google</p>
                        </a>
                        <p class="text-xs text-center text-white">
                            By registering you agree  to our term and services
                        </p>
                    </div>
                </div>
                <div class="bg-white rounded-lg inline-block shadow-lg text-center text-gray-900 z-50 p-8">
                    <div class="flex justify-end px-2">
                        <img src="img/close_modal.svg" alt="" class="modal-close cursor-pointer">
                    </div>
                    <p class="text-2xl font-bold">Register an account</p>
                    <p class="text-xs mb-4">Register an account to the platform to get started!</p>

                    <form action="" class="register-form px-4 py-6" method="POST">
                        <div classa="mb-4">
                            <input type="text" class="appearance-none rounded-lg w-full py-3 px-6 bg-gray-200 placeholder-gray-900 focus:outline-none" placeholder="Name" name="name">
                        </div>
                        <br>
                        <div classa="mb-4">
                            <input type="email" class="appearance-none rounded-lg w-full py-3 px-6 bg-gray-200 placeholder-gray-900 focus:outline-none" placeholder="Email" name="email">
                        </div>
                        <br>
                        <div classa="mb-4">
                            <input type="password" class="appearance-none rounded-lg w-full py-3 px-6 bg-gray-200 placeholder-gray-900 focus:outline-none" placeholder="Password" name="pass">
                        </div>
                        <br>
                        <div class="lg:flex flex-row-reverse justify-between">
                            <button class="bg-accent hover:bg-secondary-hover text-white font-bold py-2 px-8 rounded-full">Register</button>
                            <a href="#" class="switch-register underline text-secondary-hover text-xs">Click here to sign in!</a>
                        </div>
                    </form>

                    <div class="text-center lg:hidden">
                        <p class="my-4 text-gray-600">OR</p>
                        <a href="#" class="google-button mb-4 bg-white shadow rounded flex justify-center px-8 py-2 text-gray-900 mx-8">
                            <img src="img/google_icon.svg" alt="" class="mr-4">
                            <p class="text-xs">Sign In With Google</p>
                        </a>
                        <p class="text-xs text-center text-black">
                            By registering you agree  to our term and services
                        </p>
                    </div>
                </div>
            </div>






            <div class="sign-form grid lg:grid-cols-2">
                <div class="hidden lg:inline my-4 pt-64 px-16 relative rounded-lg shadow z-0 bg-cover bg-no-repeat" style="background-image:url('img/property_img_sample_1.png'); left:50;">
                    <div class="text-center">
                        <a href="#" class="google-button mb-4 bg-white shadow rounded flex justify-center px-8 py-2 text-gray-900 mx-8">
                            <img src="img/google_icon.svg" alt="" class="mr-4">
                            <p class="text-xs">Sign In With Google</p>
                        </a>
                        <p class="text-xs text-center text-white">
                            By login you agree  to our term and services
                        </p>
                    </div>
                </div>
                <div class="bg-white rounded-lg inline-block shadow-lg text-center text-gray-900 z-50 p-8">
                    <div class="flex justify-end px-2">
                        <img src="img/close_modal.svg" alt="" class="modal-close cursor-pointer">
                    </div>
                    <p class="text-2xl font-bold">SignIn</p>
                    <p class="text-xs mb-4">Get access to propertisin real time</p>

                    <form action="" class="signin-form px-4 py-6" method="POST">
                        <div classa="mb-4">
                            <input type="email" class="appearance-none rounded-lg w-full py-3 px-6 bg-gray-200 placeholder-gray-900 focus:outline-none" placeholder="Email" name="email">
                        </div>
                        <br>
                        <div classa="mb-4">
                            <input type="password" class="appearance-none rounded-lg w-full py-3 px-6 bg-gray-200 placeholder-gray-900 focus:outline-none" placeholder="Password" name="pass">
                        </div>
                        <br>
                        <div class="lg:flex flex-row-reverse justify-between">
                            <button class="bg-secondary-hover hover:bg-accent text-white font-bold py-2 px-8 rounded-full">SignIn</button>
                            <a href="#" class="switch-sign underline text-secondary-hover text-xs">Click here to register!</a>
                        </div>
                    </form>

                    <div class="text-center lg:hidden">
                        <p class="my-4 text-gray-600">OR</p>
                        <a href="#" class="google-button mb-4 bg-white shadow rounded flex justify-center px-8 py-2 text-gray-900 mx-8">
                            <img src="img/google_icon.svg" alt="" class="mr-4">
                            <p class="text-xs">Sign In With Google</p>
                        </a>
                        <p class="text-xs text-center text-black">
                            By registering you agree  to our term and services
                        </p>
                    </div>
                </div>
            </div>
        </div>
        
    </div>