<div class="bg-primary px-4 lg:px-16">
    <div class="grid grid-cols-2 py-8">
        <img src="img/logo_big.svg" alt="" class="w-32">
        <div class="flex justify-end">
            <button class="bg-secondary hover:bg-secondary-hover rounded px-4 py-2 text-white hidden lg:inline">Post a Shelter</button>
            <a href="#" class="modal-open">
                <div class="lg:flex lg:items-end text-white ml-4">
                    <img src="img/user_icon.svg" alt="">
                    <p>Sign In / Register</p>
                </div>
            </a>
            <div class="block md:hidden">
                <button class="menuBtn-js">
                    <img src="img/hamburger_menu.svg" alt="">
                </button>
                <button class="hidden closeBtn-js hover:text-cw-blue-600">
                    <svg class="w-6 h-6 ml-8 bg-white" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path></svg>
                </button>
            </div>
        </div>
        <div class="md:hidden">
            <nav class="hidden navMenu-js container mx-auto max-w-6xl px-5 ml-12">
            </nav>
        </div>
    </div>
</div>


<script>
    const navMenu = document.querySelector('.navMenu-js');

    const menuBtn = document.querySelector('.menuBtn-js');

    const closeBtn = document.querySelector('.closeBtn-js');

    function toggleDropDown() {
        navMenu.classList.toggle('hidden');
        menuBtn.classList.toggle('hidden');
        closeBtn.classList.toggle('hidden');
    }

    menuBtn.addEventListener('click', toggleDropDown);
    closeBtn.addEventListener('click', toggleDropDown);
</script>