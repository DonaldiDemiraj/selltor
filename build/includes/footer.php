<div class="bg-black lg:flex justify-between py-4 px-4 lg:px-16">
    <div class="mb-4">
        <p class="text-white">&copy;<a href="#" class="text-secondary">Codes</a>2021. All Rights Reserved</p>
    </div>

    <div class="flex items-center">
        <p class="text-secondary text-xs mr-4">
            <a href="#">Terms of Use</a>    
            <a href="#">Privatcy Terms</a>
        </p>

        <div class="relative">
            <select name="currecy" id="currecy" class="text-white font-bold py-3 px-4 rounded bg-opacity-50 bg-white appearance-none focus:outline-none">
                <option style="color:#FCA311;" value="Euro">€ Euro</option>
                <option style="color:#FCA311;" value="Lek"> &#x20A6; Lek</option>
                <option style="color:#FCA311;" value="USD">$ USD</option>
            </select>

            <div class="absolute flex items-center right-0 insert-y-0 px-2">
                <img src="img/pointer_down.svg" alt="">
            </div>
        </div>
    </div>
</div>