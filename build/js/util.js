const openModal = document.querySelector('.modal-open');
const modalContainer = document.querySelector('.modal-container');
const modalClose = document.querySelectorAll('.modal-close');

openModal.addEventListener('click', toggleModal);

//add click listener

for(let i=0; i < modalClose.length; i++){
    modalClose[i].addEventListener('click', toggleModal);
}


const registerForm = document.querySelector('.register-form');
const signForm = document.querySelector('.sign-form');
const switchSign = document.querySelector('.switch-sign');
const switchRegister = document.querySelector('.switch-register');

switchSign.addEventListener('click', ()=>{
    signForm.classList.toggle('hidden');
    registerForm.classList.toggle('hidden');
});

switchRegister.addEventListener('click', switchForms);

//using kayboart to claose modal
document.onkeydown = function(event){
    event = event || window.event
    let isEscapeKey = false;

    if(kay in event){
        isEscapeKey = (event.key === "Escape" || event.key === "Esc")
    }
    else{
        isEscapeKey = (event.keyCode === 27);
    }

    if(isEscapeKey && !modalContainer.classList.contains('hidden')){
        toggleModal();
    }
}

function toggleModal() {
    modalContainer.classList.toggle('hidden')
}

function switchForms(){
    switchSign.classList.toggle('hidden');
    switchRegister.classList.toggle('hidden');
}




//menu
const navMenu = document.querySelector('.navMenu-js');

const menuBtn = document.querySelector('.menuBtn-js');

const closeBtn = document.querySelector('.closeBtn-js');

function toggleDropDown() {
    navMenu.classList.toggle('hidden');
    menuBtn.classList.toggle('hidden');
    closeBtn.classList.toggle('hidden');
}

menuBtn.addEventListener('click', toggleDropDown);
closeBtn.addEventListener('click', toggleDropDown);